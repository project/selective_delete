<?php

namespace Drupal\selective_delete\Form;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Class DeleteEntityForm.
 *
 * @package Drupal\selective_delete\Form
 */
class DeleteEntityForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * DeleteEntityForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Delete Constructor.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Creating Container for constructor.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container Interface.
   *
   * @return static
   *   Return static value.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('module_handler')
    );
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'delete_selective_entity_form';
  }

  /**
   * Build the form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   * 
   * In here, we would provide the list of fields of Entities to select for deleting those entitites.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'selective_delete/selective_delete.config';

    $form['displays'] = [];
    $input = &$form_state->getUserInput();

    $form['displays']['show'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Entity Delete Settings'),
      '#tree' => TRUE,
      '#attributes' => ['class' => ['container-inline']],
    ];

    $content_entity_types = [];
    $entity_type_definations = $this->entityTypeManager->getDefinitions();
    /* @var $definition \Drupal\Core\Entity\EntityTypeInterface */
    foreach ($entity_type_definations as $definition) {
      if ($definition instanceof ContentEntityType && in_array($definition->id(), ['node', 'user'])) {
        // Allow only 'Content Types' and 'Users' to be deleted for now.
        $content_entity_types[$definition->id()] = $definition->getLabel();
      }
    }
    $form['displays']['show']['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Entity Type'),
      '#options' => $content_entity_types,
      '#empty_option' => $this->t('Please Select'),
      '#prefix' => '<div id="entity-type-wrapper">',
      '#suffix' => '</div>',
      '#size' => 1,
      '#required' => TRUE,
      '#ajax' => [
        'wrapper' => 'entity-wrapper',
        'callback' => '::EntityChangeCallback',
      ],
    ];

    $entity_options = [];
    $form['displays']['show']['entity'] = [
      '#type' => 'select',
      '#title' => $this->t('Please Select Entity'),
      '#options' => $entity_options,
      '#empty_option' => $this->t('Please Select'),
      '#prefix' => '<div id="entity-wrapper">',
      '#suffix' => '</div>',
      '#required' => TRUE,
      '#ajax' => [
        'wrapper' => 'field-wrapper',
        'callback' => '::FieldsListCallback',
      ],
    ];

    $input_entity_type = empty($input['show']['entity_type'])?"":$input['show']['entity_type'];
    $input_entity = empty($input['show']['entity'])?"":$input['show']['entity'];

    if (isset($input_entity_type) && !empty($input_entity_type) && ($input_entity_type != 'comment')) {
      $default_bundles = $this->entityTypeBundleInfo->getBundleInfo($input_entity_type);
      // If the current base table support bundles and has more than one (like user).
      if (!empty($default_bundles)) {
        // Get all bundles and their human readable names.
        foreach ($default_bundles as $type => $bundle) {
          $entity_options[$type] = $bundle['label'];
        }
        $form['displays']['show']['entity']['#options'] = $entity_options;
      }
    }

    $form['displays']['show']['fields'] = [
      '#prefix' => '<div id="field-wrapper">',
      '#suffix' => '</div>',
    ];

    // List of Allowed Data Type to be considered for deletion.
    $allowed_filter_data_type = ["boolean", "datetime", "email", "timestamp", "list_float", "list_integer", "integer", "decimal", "float", "string", "list_string", "text", "uuid", "created", "changed", "path", "serial", ];

    // Reject / Skip the un-wanted fields of Entity.
    $rejected_filter_fields = ["body", "comment", "type", "vid", "langcode", "revision_timestamp",  "revision_uid",  "revision_log", "promote", "sticky", "default_langcode", "revision_default", "revision_translation_affected", "image", "preferred_admin_langcode", "pass", "timezone", "access", "login", "init", "roles", "user_picture", ];


    if (isset($input_entity_type) && !empty($input_entity_type) && isset($input_entity) && !empty($input_entity)) {
      // Display Fields when specific Entity is Selected
      $entity_type = $input_entity_type;
      $bundle = $input_entity;
      $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, $bundle);

      $form['displays']['show']['fields']['info_markup'] = array(
        '#markup' => '<div class="info_markup">' . 
                        '<span class="info-note">' . 
                          $this->t('Note: If multiple filters are applied, by Default "and" condition will be applied !!') . 
                        '</span>' . 
                      '</div>',
        );

      foreach ($fields as $field) {
        $field_type = $field->getType();
        $field_name = $field->getName();
        $field_label = is_object($field->getLabel())?$field->getLabel()->render():$field->getLabel();

        if (in_array($field_type, $allowed_filter_data_type) && !in_array($field_name, $rejected_filter_fields)) {
          switch ($field_type) {
            case 'boolean':
              $description = $this->t("E.g. TRUE or FALSE etc.");
              break;
            case 'decimal':
            case 'float':
            case 'list_float':
              $description = $this->t("E.g. 98.76||65673.78674 etc.");
              break;
            case 'integer':
            case 'list_integer':
            case 'serial':
              $description = $this->t("E.g. 1||3214||4537 etc.");
              break;
            case 'string':
            case 'list_string':
            case 'text':
              $description = $this->t("E.g. harry||hermione||Professor Severus Snape||dumbledore||McGonagall etc.");
              break;
            case 'path':
              $description = $this->t("E.g. /hogwards/student/harry||/hogwards/professor/Severus-Snape||/hogwards/dean/dumbledore etc.");
              $description.= '<br/>' . $this->t("Each path component must have a leading slash(/)"); 
              break;
            case 'datetime':
              $description = $this->t("E.g. m/d/Y||m/d/Y - H:i:s||d/m/Y - H:i:s||Y/m/d - H:i:s etc. & it's variations");
              break;
            case 'timestamp':
            case 'created':
            case 'changed':
              $description = $this->t("E.g. 'timestamp' integer e.g. 1660338149||1664524738");
              break;
            case 'email':
              $description = $this->t("E.g. harry@hogwarts.org||hermione@hogwarts.com etc. & it's variations");
              break;
            case 'uuid':
              $description = $this->t("E.g. da8e0948-08d3-466b-b0ca-451857149909||bd51e975-a0a3-43bb-865e-2a01bca04adf etc.");
              break;
            default:
              $description = $this->t("Need To Fix For") . " '" . $field_type . "' ! " . $this->t("Please Raise a Ticket to consider this datatype !!");
              break;
          }

          $field_label = ($field_name === 'nid')?"Nid":$field_label;
          // Textarea.
          $form['displays']['show']['fields'][$field_type][$field_name] = [
            '#type' => 'textarea',
            '#title' => $field_label . $this->t(" Filter "),
            '#description' => $description,
          ];
        }
      }
    }

    $msg = '<div class="info_markup">' .
              '<span class="info-note">' . 
                $this->t('Allowed Filter Data Type') . ': ' . implode(", ", $allowed_filter_data_type) . 
              '</span><br /><br />' .
              '<span class="info-warning">' . 
                $this->t('Note: <b>Use Selective Delete cautiously !</b> There is no turning back once records are deleted off of Databse !!') . 
              '</span><br />' .
            '</div>';
    $form['message'] = [ '#markup' => $msg, ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Delete',
    ];
    return $form;
  }
 
  /**
   * {@inheritdoc}
   */
  public function EntityChangeCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand("#entity-wrapper", $form['displays']['show']['entity']));
    $response->addCommand(new HtmlCommand("#field-wrapper", ""));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function FieldsListCallback(array &$form, FormStateInterface $form_state) {
    return $form['displays']['show']['fields'];
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_values = $form_state->getValues();
    $empty_field = TRUE;
    foreach ($form_values as $key => $field_show) {
      if ($key === 'show') {
        foreach ($field_show as $k => $v) {
          if (($k == 'entity_type' || $k == 'entity') && empty($v)) {
            $form_state->setErrorByName('entity_type', $this->t('Please select Entity Type && Entity.'));
          } elseif($k == 'fields') {
            foreach ($v as $field_type => $fields) {
              foreach ($fields as $field_name => $field_value) {
                if (!empty($field_value)) {
                  $empty_field = FALSE;
                  break;
                }
              }
            }
          }
        }
      }
    }
    if ($empty_field) {
      $form_state->setErrorByName('entity_type', $this->t('Please enter atleast one Filter to Delete entities.'));
    }
  }

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get $form_state values.
    $values = $form_state->getValues();
    // Entity type.
    $entity_type = $values['show']['entity_type'];
    // Get bundle.
    $bundle = $values['show']['entity'];
    $url = Url::fromRoute('selective_delete.delete_conditional_entities_confirmation', [
      'values' => base64_encode(serialize($values)),
    ]);
    $form_state->setRedirectUrl($url);
  }
}

