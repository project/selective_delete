<?php

namespace Drupal\selective_delete\Form;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\Core\StringTranslation\TranslatableMarkup;


/**
 * Defines a confirmation form for deleting selective Entities.
 * 
 * @package Drupal\selective_delete\Form
 */

class DeleteEntityConfirmationForm extends ConfirmFormBase {
  /**
   * {@inheritdoc}
   */
  protected $queryValues;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $query = $this->getRequest()->query->get('values');
    if (isset($query) && !empty($query)) {
      $this->queryValues = @unserialize(base64_decode($query));
      if (!$this->queryValues) {
        throw new NotFoundHttpException();
      }
    } else {
      throw new NotFoundHttpException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_selective_entity_confirmation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t("Are you sure you want to delete those '%entity(s)' of type '%entity_type' ?", [
      '%entity' => $this->queryValues['show']['entity'],
      '%entity_type' => $this->queryValues['show']['entity_type'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): TranslatableMarkup {
    return $this->t("This action cannot be undone. It's recommended to take a backup of Database first !!");
  }

  /**
   * {@inheritdoc}
   *
   * If the delete command is canceled, return to the contact list.
   */
  public function getCancelUrl(): Url {
    return new Url('selective_delete.delete_conditional_entities');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText(): TranslatableMarkup {
    return $this->t('Cancel');
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if(!array_filter($this->queryValues['show']['fields'])) {
      $form_state->setErrorByName('entity_type', $this->t('It appears that Filter to Delete entities is still missing.'));
    }   
  }

  /**
   * {@inheritdoc}
   *
   * Delete the entity and log the event. logger() replaces the watchdog.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if(array_filter($this->queryValues['show']['fields'])) {
      $conditions_applied = 0;
      $entity_ids = $cleaned_path_ids = [];
      $notice = $status = "";
      $query = \Drupal::entityQuery($this->queryValues['show']['entity_type']);

      ($this->queryValues['show']['entity_type'] == 'node')?$query->condition('type', $this->queryValues['show']['entity']):$query->condition('uid', [0,1], "NOT IN");

      foreach ($this->queryValues['show']['fields'] as $field_type => $fields_list) {
        if (is_array($fields_list) && !empty($fields_list)) {
          foreach ($fields_list as $field_name => $field_values) {
            if (!empty($field_values)) {

              if ($field_type == 'boolean') {
                $field_values = str_ireplace("FALSE", 0, str_ireplace("TRUE", 1, $field_values));
              }

              $arr_field_values = array_filter(explode("||", $field_values), fn($value) => !is_null($value) && $value !== '');
              $arr_field_values = array_map('trim', $arr_field_values);

              if ($field_type == 'path') {
                $cleaned_path_ids = $this->cleanup_paths($this->queryValues['show']['entity_type'], $arr_field_values);
              } else {
                $query->condition($field_name, $arr_field_values, 'IN');
                $conditions_applied++;
              }
            }
          }
        }
      }

      if ($conditions_applied > 0) {
        $entity_ids = $query->execute();
        if (!empty($entity_ids)) {
          $storage_handler = \Drupal::entityTypeManager()->getStorage($this->queryValues['show']['entity_type']);
          $entities = $storage_handler->loadMultiple($entity_ids);
          $storage_handler->delete($entities);
        }
      }

      $entity_ids = array_merge($entity_ids, $cleaned_path_ids);

      if (!empty($entity_ids) && count($entity_ids) > 0) {
        $notice = $this->t("@type: deleted Entity Ids : %ids.", [
                            '@type' => $this->queryValues['show']['entity'],
                            '%ids' => implode(', ', $entity_ids),
                          ]);
        $status = $this->t('Entities were successfully deleted !');
      } else {
        $notice = $this->t("@type: No Records were found for deletion.", [
                            '@type' => $this->queryValues['show']['entity'],
                          ]);
        $status = $this->t('No Records were found for deletion !');
      }

      $this->logger('selective_delete')->notice($notice);
      $this->messenger()->addStatus($status);
    }

    $form_state->setRedirect('selective_delete.delete_conditional_entities');
  }

  /**
   * {@inheritdoc}
   *
   * Delete the entity and log the event. logger() replaces the watchdog.
   */
  public function cleanup_paths($entity_type, $arr_field_values) {
   $deleted_nids = [];
   foreach ($arr_field_values as $key => $path) {
      // Check if path has leading slash (/) before moving ahead.
      if (substr($path, 0, 1) == "/") {
        // Check the path is valid before proceeding.
        $pathIsValid = \Drupal::pathValidator()->isValid($path);
        if ($pathIsValid) {
          $alias = \Drupal::service('path_alias.manager')->getPathByAlias($path);
          if(isset($alias) && !empty($alias)) {
            $params = Url::fromUri("internal:" . $alias)->getRouteParameters();
            // Get nid.
            $nid = is_array($params) && isset($params[$entity_type])?$params[$entity_type]:"";
            if ($nid) {
              $node = Node::load($nid);
              // Check if node exists with the given nid.
              if ($node) {
                $node->delete();
                $deleted_nids[] = $nid;
              }
            }
          }
        }
      }
   }
   return $deleted_nids;
  }
}

