CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module provides a way to delete the Selective Entities in Bulk.

Though we already have "Bulk Delete" Module available to delete the nodes of specific content type, however, limitation with that module is either you 
have to delete ALL or NONE, and that too, Nodes of specific content type only !

What if I want to delete ALL the Nodes with specific content type having particular value for specific field in those nodes ? OR say, I want to 
delete list of Users with specific first name ? Even with the Dashboard view, say suppose you have 5000+ records, and you want to delete only 1000 
records that have specific value in it's specific field. You would have to either open each node to check the value of field and then delete OR at max, 
if field value is shown in view, then select those 1000 records one by one for bulk deletion.

These and many such problems where there is no middle way, like extra filter conditions for specific selected nodes OR users, are covered with 
"Delete Selective Entities" module to ease the record management.

Note: Use Selective Delete cautiously ! There is no turning back once records are deleted off of Databse !!

This action cannot be undone so, it's recommended to take a backup of Database first !!

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/selective_delete

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/selective_delete

REQUIREMENTS
------------

This module does not depend on any other module.  So, feel free to use at your discretion.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will allow you to select the Entitites and then their respective fields in order to delete the 
records off of database.

MAINTAINERS
-----------

Current maintainers:
 * Sandip V. Tekale (sandip27) - https://www.drupal.org/user/2804167
